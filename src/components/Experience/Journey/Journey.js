import React from 'react';
import './Journey.less';
import PropTypes from 'prop-types';

export default class Journey extends React.Component {
  render() {
    const { year, title, description } = this.props.education;
    return (
      <article className="journey">
        <h4>{year}</h4>
        <section>
          <h5>{title}</h5>
          <p>{description}</p>
        </section>
      </article>
    );
  }
}

Journey.propTypes = {
  education: PropTypes.object
};
