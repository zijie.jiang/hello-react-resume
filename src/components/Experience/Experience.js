import React from 'react';
import Tag from './Tag/Tag';
import Journey from './Journey/Journey';
import './Experience.less';
import PropTypes from 'prop-types';

export default class Experience extends React.Component {
  render() {
    const { description, educations = [] } = this.props;
    return (
      <article className="container">
        <Tag value="ABOUT ME" />
        <p>{description}</p>
        <Tag value="EDUCATION" />
        <ul>
          {educations.map((education, index) => (
            <li key={index}>
              <Journey education={education} />
            </li>
          ))}
        </ul>
      </article>
    );
  }
}

Experience.propTypes = {
  description: PropTypes.string,
  educations: PropTypes.array
};
