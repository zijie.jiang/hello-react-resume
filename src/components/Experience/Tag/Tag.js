import React from 'react';
import PropTypes from 'prop-types';

export default class Tag extends React.Component {
  render() {
    const { value } = this.props;
    return <h3>{value}</h3>;
  }
}

Tag.propTypes = {
  value: PropTypes.string
};
