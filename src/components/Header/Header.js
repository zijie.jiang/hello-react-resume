import React from 'react';
import Avatar from '../../assets/avatar.jpg';
import './Header.less';
import PropTypes from 'prop-types';

export default class Header extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { name, age } = this.props;
    return (
      <header>
        <img src={Avatar} alt="avatar" />
        <h1>Hello,</h1>
        <h2>
          my name is {name} {age}yo and this is my resume/cv
        </h2>
      </header>
    );
  }
}

Header.propTypes = {
  name: PropTypes.string,
  age: PropTypes.number
};
