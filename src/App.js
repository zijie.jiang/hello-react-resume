import React, { Component } from 'react';
import './App.less';
import Header from './components/Header/Header';
import Experience from './components/Experience/Experience';

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      data: {}
    };
  }

  render() {
    const { name, age, description, educations } = this.state.data;
    return (
      <main className="app">
        <Header name={name} age={age} />
        <Experience description={description} educations={educations} />
      </main>
    );
  }
  componentDidMount() {
    const URL = 'http://localhost:3000/person';
    fetch(URL)
      .then(response => response.json())
      .then(json => {
        const { name, age, description, educations } = json;
        this.setState({
          data: {
            name: name,
            age: age,
            description: description,
            educations: educations
          }
        });
      });
  }
}

export default App;
